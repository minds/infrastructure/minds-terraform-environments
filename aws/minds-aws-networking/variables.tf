/* Global */

variable "regions" {
  description = "AWS regions in which to deploy the resources for the environment."
  type        = list(string)

  default = ["us-east-1"]
}

/* VPC */

variable "vpc_name_prefix" {
  description = "VPC name prefix."
  type        = map(string)

  default = {
    default = "minds-eks"
    sandbox = ""
  }
}

variable "vpc_cidr" {
  description = "CIDR block to associate with the deployed VPC."
  type        = map(string)

  default = {
    default = "192.168.0.0/16" // TODO need to appropriately size the VPC for production
    sandbox = ""
  }
}

variable "vpc_azs" {
  description = "AWS availibility zones in which to deploy."
  type        = map(list(string))

  default = {
    default = ["us-east-1a", "us-east-1b", "us-east-1d"] // TODO ensure the correct AZ configuration for production
    sandbox = []
  }
}

variable "vpc_private_subnets" {
  description = "CIDR blocks for each of the deployed private subnets."
  type        = map(list(string))

  default = {
    default = ["192.168.96.0/19", "192.168.128.0/19", "192.168.160.0/19"]
    sandbox = []
  }
}

variable "vpc_public_subnets" {
  description = "CIDR blocks for each of the deployed public subnets."
  type        = map(list(string))

  default = {
    default = ["192.168.0.0/19", "192.168.32.0/19", "192.168.64.0/19"]
    sandbox = []
  }
}
