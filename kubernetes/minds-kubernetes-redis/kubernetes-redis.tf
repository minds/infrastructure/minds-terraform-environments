resource "kubernetes_namespace" "caching" {
  metadata {
    name        = "caching-new"
    annotations = { name = "caching-new" }
    labels      = { name = "caching-new" }
  }
}

resource "helm_release" "redis" {
  name       = "redis"
  chart      = "redis"
  namespace  = kubernetes_namespace.caching.metadata[0].name
  repository = "https://charts.bitnami.com/bitnami"

  values = [
    yamlencode({
      auth = { enabled = false }
      metrics = {
        enabled        = true,
        serviceMonitor = { enabled = true }
      }
      master = {
        persistence     = { enabled = false }
        resources       = { requests = { cpu = "200m", memory = "256Mi" } } // TODO adjust these for production
        extraFlags      = ["--maxmemory 8gb", "--maxmemory-policy allkeys-lfu"]
        disableCommands = []
      }
      replica = {
        persistence    = { enabled = false }
        replicaCount   = 1
        resources      = { requests = { cpu = "200m", memory = "256Mi" } } // TODO adjust these for production
        extraFlags     = ["--maxmemory 8gb", "--maxmemory-policy allkeys-lfu"]
        readinessProbe = { initialDelaySeconds = "60" }
      }
    }),
    <<EOT
commonConfiguration: |-
  appendonly no
  save ""
EOT
  ]
}
