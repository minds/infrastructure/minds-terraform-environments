terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.16.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  // Having issues with this, might need to tag the VPC resources individually
  // https://github.com/hashicorp/terraform-provider-aws/issues/19583
  # default_tags {
  #   tags = {
  #     Environment = "minds-aws-networking"
  #     Automation  = "terraform"
  #   }
  # }
}
