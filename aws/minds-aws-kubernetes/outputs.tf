output "eks_cluster_id" {
  description = "Unique ID for the EKS cluster."
  value       = module.eks.cluster_id
}