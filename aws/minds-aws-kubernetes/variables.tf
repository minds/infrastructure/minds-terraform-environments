variable "cluster_name" {
  description = "Name for the deployed EKS cluster."
  type        = string
}

variable "vpc_id" {
  description = "VPC id in which to deploy the EKS cluster."
  type        = string
}

variable "cluster_subnets" {
  description = "VPC subnets in which to deploy the EKS cluster and node groups."
  type        = list(string)
}
