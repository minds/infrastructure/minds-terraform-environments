// TODO should perhaps move Consul to a seperate module
resource "helm_release" "consul" {
  name      = "consul"
  namespace = kubernetes_namespace.vault.metadata[0].name

  repository = "https://helm.releases.hashicorp.com"
  chart      = "consul"
  version    = "0.31.1"

  values = [yamlencode({
    global = { image = var.consul_image },
    server = {
      replicas        = var.consul_replicas,
      bootstrapExpect = var.consul_replicas
    }
    resources = {
      requests = { memory = "512Mi" },
      limits   = { memory = "726Mi" }
    }
  })]
}
