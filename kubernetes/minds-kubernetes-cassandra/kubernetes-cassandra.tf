resource "kubernetes_namespace" "cassandra" {
  metadata {
    name        = "cassandra"
    annotations = { name = "cassandra" }
    labels      = { name = "cassandra" }
  }
}

resource "helm_release" "cassandra" {
  name       = "cassandra"
  chart      = "cassandra"
  namespace  = kubernetes_namespace.cassandra.metadata[0].name
  repository = "https://charts.bitnami.com/bitnami"

  values = [
    yamlencode({
      image = { tag = "3.11.13-debian-10-r19" },
      resources = {
        requests = { cpu = "1", memory = "2Gi" },
        limits   = { cpu = "2", memory = "4Gi" }
      }
    })
  ]
}
