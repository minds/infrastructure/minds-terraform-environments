dependency "network" {
  config_path = "../minds-aws-networking"

  mock_outputs = {
    eks_vpc_name               = "temporary-dummy-name"
    eks_vpc_id                 = "temporary-dummy-id"
    eks_vpc_private_subnet_ids = ["one", "two", "three"]
  }
}

inputs = {
  cluster_name    = dependency.network.outputs.eks_vpc_name
  vpc_id          = dependency.network.outputs.eks_vpc_id
  cluster_subnets = dependency.network.outputs.eks_vpc_private_subnet_ids
}
