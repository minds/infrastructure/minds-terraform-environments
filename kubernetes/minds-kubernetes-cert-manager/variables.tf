variable "cluster_id" {
  description = "Target EKS cluster id."
  type        = string
}
