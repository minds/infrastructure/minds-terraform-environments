locals {
  oidc_url = replace(data.aws_eks_cluster.this.identity.0.oidc.0.issuer, "https://", "")
}

resource "aws_kms_key" "vault_auto_unseal" {
  description = "Vault Auto Unseal (aws)"
}

resource "aws_iam_role" "vault_aws_role" {
  assume_role_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::324044571751:oidc-provider/${local.oidc_url}"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "${local.oidc_url}:sub": "system:serviceaccount:vault:vault"
        }
      }
    }
  ]
}
EOF
  path               = "/"
}

resource "aws_iam_policy" "vault_aws_kms" {
  description = ""
  path        = "/service-role/"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "kms:*",
            "Resource": "${aws_kms_key.vault_auto_unseal.arn}"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "vault_aws_kms" {
  role       = aws_iam_role.vault_aws_role.name
  policy_arn = aws_iam_policy.vault_aws_kms.arn
}
