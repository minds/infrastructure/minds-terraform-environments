resource "kubernetes_namespace" "elasticsearch" {
  metadata {
    name        = "elasticsearch"
    annotations = { name = "elasticsearch" }
    labels      = { name = "elasticsearch" }
  }
}

resource "helm_release" "elasticsearch" {
  name      = "elasticsearch"
  chart     = "./opendistro-es-1.13.3.tgz"
  namespace = kubernetes_namespace.elasticsearch.metadata[0].name

  values = [yamlencode({
    global = {
      clusterName   = "minds-k8s"
      imageRegistry = "public.ecr.aws/t7r8r4d6"
    },
    elasticsearch = {
      client = {
        replicas = var.client_replicas,
        javaOpts = "-Xms2G -Xmx2G"
        resources = {
          requests = { cpu = "600m", memory = "2Gi" },
          limits   = { cpu = "1", memory = "4Gi" }
        }
      },
      master = {
        replicas = var.master_replicas,
        javaOpts = "-Xms${var.master_jvm_memory_gb}G -Xmx${var.master_jvm_memory_gb}G",
        resources = {
          requests = { cpu = "1", memory = "${var.master_jvm_memory_gb * 2}Gi" },
          limits   = { cpu = "2", memory = "${var.master_jvm_memory_gb * 2}Gi" }
        }
      },
      data = {
        replicas = var.data_replicas,
        javaOpts = "-Xms${var.data_jvm_memory_gb}G -Xmx${var.data_jvm_memory_gb}G",
        resources = {
          requests = {
            cpu    = "${var.data_jvm_memory_gb / 5}", # 0.2 cpus per GB of RAM
            memory = "${var.data_jvm_memory_gb * 2}Gi"
          },
          limits = {
            cpu    = "${var.data_jvm_memory_gb / 5}",
            memory = "${var.data_jvm_memory_gb * 2}Gi"
          }
        }
      }
    }
  })]
}
