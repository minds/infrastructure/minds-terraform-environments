dependency "eks_cluster" {
  config_path = "../../aws/minds-aws-kubernetes"

  mock_outputs = {
    eks_cluster_id = "temporary-dummy-name"
  }
}

inputs = {
  cluster_id = dependency.eks_cluster.outputs.eks_cluster_id
}
