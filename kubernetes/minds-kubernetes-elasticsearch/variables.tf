variable "cluster_id" {
  description = "Target EKS cluster id."
  type        = string
}

variable "master_jvm_memory_gb" {
  type    = number
  default = 1
}

variable "data_jvm_memory_gb" {
  type    = number
  default = 1
}

variable "master_replicas" {
  type    = number
  default = 1
}

variable "client_replicas" {
  type    = number
  default = 1
}

variable "data_replicas" {
  type    = number
  default = 1
}

variable "reindex_host" {
  type    = string
  default = "elasticsearch-master.default.svc.cluster.local:9200"
}