dependency "eks_cluster" {
  config_path = "../../aws/minds-aws-kubernetes"

  mock_outputs = {
    eks_cluster_id = "temporary-dummy-name"
  }
}

dependency "cert_manager" {
  config_path  = "../minds-kubernetes-cert-manager"
  skip_outputs = true
}

inputs = {
  cluster_id = dependency.eks_cluster.outputs.eks_cluster_id
}
