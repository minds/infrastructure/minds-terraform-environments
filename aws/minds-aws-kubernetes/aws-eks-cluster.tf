module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.23.0"

  // Network
  vpc_id     = var.vpc_id
  subnet_ids = var.cluster_subnets

  // Control Plane
  cluster_name    = var.cluster_name
  cluster_version = "1.19"

  // We'll restrict East/West traffic with NetworkPolicy, so we can allow connections between nodes here
  node_security_group_additional_rules = {
    node_egress = {
      description = "Node to node egress"
      protocol    = "all"
      from_port   = 0
      to_port     = 65535
      type        = "egress"
      self        = true
    },
    node_ingress = {
      description = "Node to node ingress"
      protocol    = "all"
      from_port   = 0
      to_port     = 65535
      type        = "ingress"
      self        = true
    }
  }

  // Data Plane
  // TODO break out node groups
  eks_managed_node_groups = { for i, subnet in var.cluster_subnets : "worker-${i}" => {
    name                      = "worker-${i}"
    subnet_ids                = [subnet]
    instance_types            = ["t3a.medium"]
    desired_capacity          = 3
    min_capacity              = 1
    max_capacity              = 3
    disk_size                 = 50
    source_security_group_ids = []
  } }
}
