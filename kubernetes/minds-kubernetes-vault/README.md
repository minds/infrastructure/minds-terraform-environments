# Vault

Key management for Minds. Depends on cert-manager.

### Initialising

`kubectl -n vault exec -it pod/vault-0 -- vault operator init`

### Accessing the UI

```
kubectl -n vault port-forward vault-0 8200:8200
```

Then visit: http://localhost:8200/ui

## Kubernetes Service Account

You will need to access the `vault-0` shell:

```
kubectl -n vault exec -it vault-0 -- /bin/sh
vault login
```


### Create k8s auth method with vault

```
vault auth enable kubernetes
vault write auth/kubernetes/config \
    token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
    kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
    kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
```

### Create a role for your deployments (eg. minds-engine) kubernetes service group

```
vault write auth/kubernetes/role/eks-cluster \
    bound_service_account_names=minds-engine,vault-certs-issuer \
    bound_service_account_namespaces=default,cert-manager \
    policies=default,eks-cluster-pki \
    ttl=1h
```

(Note. Ensure the above `bound_service_account_names` are provided in `eks-k8s.tf`)

See https://learn.hashicorp.com/tutorials/vault/kubernetes-sidecar for more information about mounting secrets on your deployments.

## User management

`vault write auth/userpass/users/USERNAME password=newpass`

## Using with cert-manager

See https://learn.hashicorp.com/tutorials/vault/kubernetes-cert-manager?in=vault/kubernetes for more information.

You will need to access the `vault-0` shell:

```
kubectl -n vault exec -it vault-0 -- /bin/sh
vault login
```

```
vault secrets enable pki
```

### Extend the certificate lease to 1 year

```
vault secrets tune -max-lease-ttl=8760h pki
```

### Import our intermediate CA

```
vault write pki/config/ca pem_bundle=@/tmp/bundle.pem
```


### Create a new pki role so we can get certficates

**Change to .minds.io if using sandboxes**

```
vault write pki/roles/eks-cluster \
    allowed_domains=infra.minds.com,elasticsearch.svc.cluster.local \
    allow_subdomains=true \
    max_ttl=8765h
```

### Update our policy so we can read and create the certificates

```
vault policy write eks-cluster-pki - <<EOF
path "pki*"                      { capabilities = ["read", "list"] }
path "pki/roles/eks-cluster"   { capabilities = ["create", "update"] }
path "pki/sign/eks-cluster"    { capabilities = ["create", "update"] }
path "pki/issue/eks-cluster"   { capabilities = ["create"] }
EOF
```

