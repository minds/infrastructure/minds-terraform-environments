resource "kubernetes_namespace" "vault" {
  metadata {
    annotations = { name = "vault" }
    labels      = { name = "vault" }
    name        = "vault"
  }
}

locals {
  vault_values = {
    server = {
      nodeSelector = { stateful = "true" }
      tolerations = [
        {
          key      = "stateful"
          operator = "Equal"
          value    = "true"
          effect   = "NoSchedule"
        }
      ]
    }
  }
}

resource "helm_release" "vault" {
  name      = "vault"
  namespace = kubernetes_namespace.vault.metadata[0].name

  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  version    = "0.14.0"

  values = [yamlencode({
    namespace = kubernetes_namespace.vault.metadata[0].name
    injector = {
      image      = { repository = var.vault_injector_image },
      agentImage = { repository = var.vault_injector_agent_image }
    },
    server = {
      ingress = {
        enabled     = var.vault_ingress_enabled,
        annotations = { "kubernetes.io/ingress.class" = "traefik" },
        hosts       = [{ host = var.vault_ingress_domain }]
      },
      image = { repository = var.vault_server_image },
      ha = {
        enabled = var.vault_ingress_enabled,
        config  = <<EOT
ui = true

listener "tcp" {
  tls_disable = 1
  address = "[::]:8200"
  cluster_address = "[::]:8201"
}

seal "awskms" {
  region     = "us-east-1"
  kms_key_id = "${aws_kms_key.vault_auto_unseal.key_id}"
}

storage "consul" {
  path = "vault"
  # address = "HOST_IP:8500"
  address = "http://consul-consul-server.vault.svc:8500"
}
EOT
      }
    }
  })]
}
