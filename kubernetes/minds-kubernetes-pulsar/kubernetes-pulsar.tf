resource "kubernetes_namespace" "pulsar" {
  metadata {
    name        = "pulsar"
    annotations = { name = "pulsar" }
    labels      = { name = "pulsar" }
  }
}


resource "helm_release" "pulsar" {
  name      = "pulsar"
  namespace = kubernetes_namespace.pulsar.metadata[0].name

  repository = "https://pulsar.apache.org/charts"
  chart      = "pulsar"
  version    = "2.7.6"

  timeout = 1800

  values = [yamlencode({
    namespace  = kubernetes_namespace.pulsar.metadata[0].name,
    initialize = true,
    components = { autorecovery = true },
    zookeeper = {
      replicaCount = 1,
      podMonitor   = { enabled = var.enable_monitoring }
    },
    bookkeeper = { replicaCount = 1 },
    broker = {
      replicaCount                 = 1,
      podMonitor                   = { enabled = var.enable_monitoring },
      restartPodsOnConfigMapChange = true,
      resources                    = { requests = { cpu = "1", memory = "2048Mi" } },
      configData = {
        autoSkipNonRecoverableData       = "true",
        managedLedgerDefaultEnsembleSize = "1",
        managedLedgerDefaultWriteQuorum  = "1",
        managedLedgerDefaultAckQuorum    = "1"
        PULSAR_MEM                       = "-Xms1024m -Xmx1024m -XX:MaxDirectMemorySize=2048m"
      },
    },
    proxy = {
      replicaCount                 = 1,
      podMonitor                   = { enabled = var.enable_monitoring },
      restartPodsOnConfigMapChange = true
      resources                    = { requests = { cpu = "1", memory = "2048Mi" } },
      configData = {
        PULSAR_MEM                      = "-Xms1024m -Xmx1024m -XX:MaxDirectMemorySize=2048m",
        maxConcurrentInboundConnections = "50000"
      }
    },
    monitoring = {
      grafana       = false,
      prometheus    = false,
      node_exporter = false,
      alert_manager = false
    }
  })]
}
