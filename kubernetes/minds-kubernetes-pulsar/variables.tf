variable "cluster_id" {
  description = "Target EKS cluster id."
  type        = string
}

variable "enable_monitoring" {
  description = "Enable monitoring with Prometheus."
  type        = bool

  default = true
}
