# Minds AWS Networking

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~>4.16.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.16.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc_eks_main"></a> [vpc\_eks\_main](#module\_vpc\_eks\_main) | terraform-aws-modules/vpc/aws | 3.14.0 |

## Resources

| Name | Type |
|------|------|
| [random_string.suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [aws_caller_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_eks_cluster_name_prefix"></a> [eks\_cluster\_name\_prefix](#input\_eks\_cluster\_name\_prefix) | EKS cluster name prefix for use with tagging the main EKS VPC. | `map(string)` | <pre>{<br>  "default": "minds-eks",<br>  "sandbox": ""<br>}</pre> | no |
| <a name="input_regions"></a> [regions](#input\_regions) | AWS regions in which to deploy the resources for the environment. | `list(string)` | <pre>[<br>  "us-east-1"<br>]</pre> | no |
| <a name="input_vpc_azs"></a> [vpc\_azs](#input\_vpc\_azs) | AWS availibility zones in which to deploy. | `map(list(string))` | <pre>{<br>  "default": [<br>    "us-east-1a",<br>    "us-east-1b",<br>    "us-east-1c"<br>  ],<br>  "sandbox": []<br>}</pre> | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | CIDR block to associate with the deployed VPC. | `map(string)` | <pre>{<br>  "default": "192.168.0.0/16",<br>  "sandbox": ""<br>}</pre> | no |
| <a name="input_vpc_private_subnets"></a> [vpc\_private\_subnets](#input\_vpc\_private\_subnets) | CIDR blocks for each of the deployed private subnets. | `map(list(string))` | <pre>{<br>  "default": [<br>    "192.168.96.0/19",<br>    "192.168.128.0/19",<br>    "192.168.160.0/19"<br>  ],<br>  "sandbox": []<br>}</pre> | no |
| <a name="input_vpc_public_subnets"></a> [vpc\_public\_subnets](#input\_vpc\_public\_subnets) | CIDR blocks for each of the deployed public subnets. | `map(list(string))` | <pre>{<br>  "default": [<br>    "192.168.0.0/19",<br>    "192.168.32.0/19",<br>    "192.168.64.0/19"<br>  ],<br>  "sandbox": []<br>}</pre> | no |

## Outputs

No outputs.
