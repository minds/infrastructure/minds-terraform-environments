resource "random_string" "suffix" {
  length  = 8
  special = false
}

module "vpc_eks_main" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.0"

  name = "${var.vpc_name_prefix[terraform.workspace]}-${random_string.suffix.result}"

  // Network
  cidr = var.vpc_cidr[terraform.workspace]
  azs  = var.vpc_azs[terraform.workspace]

  private_subnets = var.vpc_private_subnets[terraform.workspace]
  public_subnets  = var.vpc_public_subnets[terraform.workspace]

  enable_nat_gateway   = true
  single_nat_gateway   = true // TODO revisit this, single NAT gateway contributes to cross-az data transfer costs
  enable_dns_hostnames = true

  // Tagging
  tags = { "kubernetes.io/cluster/${var.vpc_name_prefix[terraform.workspace]}-${random_string.suffix.result}" = "shared" }
  public_subnet_tags = {
    "kubernetes.io/cluster/${var.vpc_name_prefix[terraform.workspace]}-${random_string.suffix.result}" = "shared"
    "kubernetes.io/role/elb"                                                                           = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${var.vpc_name_prefix[terraform.workspace]}-${random_string.suffix.result}" = "shared"
    "kubernetes.io/role/internal-elb"                                                                  = "1"
  }
}
