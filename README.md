# Minds Terraform Environments

Minds Infrastructure as Code Environments

***Note*** This repository is a work in progress and is not being used in production. For our live infrastructure, see [this repo](https://gitlab.com/minds/infrastructure/minds-terraform).

## Getting started

### pre-commit

This repository utilizes [Gruntwork's hooks](https://github.com/gruntwork-io/pre-commit) for [pre-commit](https://pre-commit.com/) for formatting and linting. In order to make changes, you'll first need to install `pre-commit` and [tflint](https://github.com/terraform-linters/tflint). If you're using Homebrew, you can install with:

```bash
brew install pre-commit tflint
```

### Terragrunt

We manage our Terraform modules with [Terragrunt](https://terragrunt.gruntwork.io/). This allows us to create dependencies between modules and deploy multiple with a single command. 

You can install Terragrunt with [Homebrew](https://brew.sh):

```
brew install terragrunt
```

In order to deploy the entire stack, we can simply run:

```
terragrunt run-all apply
```

You can use `terragrunt` to run other commands on all modules, for example:

```
terragrunt run-all validate
```

Some modules may require manual intervention after deployment (eg: Vault).

#### Manage Module Dependencies

You can define module dependencies (including chaining inputs and outputs) in the `terragrunt.hcl` file found in each module. For example, if we want to deploy an application onto EKS in AWS, we can create a dependency and retrieve the name of the EKS cluster through it's outputted value:

```hcl
dependency "eks_cluster" {
  config_path = "../../aws/minds-aws-kubernetes"

  mock_outputs = {
    eks_cluster_id = "temporary-dummy-name"
  }
}

dependency "cert_manager" {
  config_path  = "../minds-kubernetes-cert-manager"
  skip_outputs = true
}

inputs = {
  cluster_id = dependency.eks_cluster.outputs.eks_cluster_id
}
```

## Documentation

Each environment is documented using [terraform-docs](https://github.com/terraform-docs/terraform-docs). This is currently manually executed, we should however automate this.

```bash
brew install terraform-docs

terraform-docs markdown $ENVIRONMENT_FOLDER
```
