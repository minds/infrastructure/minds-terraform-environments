output "eks_vpc_name" {
  description = "Name for the main EKS VPC."
  value       = module.vpc_eks_main.name
}

output "eks_vpc_id" {
  description = "Unique ID for the main EKS VPC."
  value       = module.vpc_eks_main.vpc_id
}

output "eks_vpc_private_subnet_ids" {
  description = "Unique IDs for the main EKS VPC's private subnets."
  value       = module.vpc_eks_main.private_subnets
}

output "eks_vpc_public_subnet_ids" {
  description = "Unique IDs for the main EKS VPC's public subnets."
  value       = module.vpc_eks_main.public_subnets
}
