resource "kubernetes_service_account" "vault_certs_issuer" {
  metadata {
    name      = "vault-certs-issuer"
    namespace = "cert-manager"
  }
}

resource "kubernetes_manifest" "vault_cluster_issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1alpha2"
    kind       = "ClusterIssuer"
    metadata   = { name = "vault-certs-issuer" }
    spec = {
      vault = {
        server = "http://vault.vault:8200"
        path   = "pki/sign/eks-cluster"
        auth = {
          kubernetes = {
            mountPath = "/v1/auth/kubernetes"
            role      = "eks-cluster"
            secretRef = {
              name = kubernetes_service_account.vault_certs_issuer.default_secret_name
              key  = "token"
            }
          }
        }
      }
    }
  }
}
