variable "cluster_id" {
  description = "Target EKS cluster id."
  type        = string
}

##################
##### Consul #####
##################

variable "consul_replicas" {
  description = "Number of Consul replicas to deploy."
  type        = number

  default = 1
}

variable "consul_image" {
  description = "Docker image to use for Consul."
  type        = string

  default = "public.ecr.aws/hashicorp/consul:1.10.1"
}

#################
##### Vault #####
#################

variable "vault_injector_image" {
  description = "Docker image to use for Vault injector."
  type        = string

  default = "public.ecr.aws/t7r8r4d6/hashicorp/vault-k8s"
}

variable "vault_injector_agent_image" {
  description = "Docker image to use for Vault injector agent."
  type        = string

  default = "public.ecr.aws/t7r8r4d6/hashicorp/vault"
}

variable "vault_server_image" {
  description = "Docker image to use for Vault."
  type        = string

  default = "public.ecr.aws/t7r8r4d6/hashicorp/vault"
}

variable "vault_ingress_enabled" {
  description = "Whether to enable Ingress to Vault."
  type        = bool

  default = false
}

variable "vault_ingress_domain" {
  description = "The domain name for the vault"
  type        = string

  default = "vault.minds.io"
}

