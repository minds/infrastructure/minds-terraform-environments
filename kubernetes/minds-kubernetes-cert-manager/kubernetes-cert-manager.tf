resource "kubernetes_namespace" "cert_manager" {
  metadata {
    annotations = { name = "cert-manager" }
    labels      = { name = "cert-manager" }
    name        = "cert-manager"
  }
}

resource "helm_release" "cert-manager" {
  name      = "cert-manager"
  namespace = kubernetes_namespace.cert_manager.metadata[0].name

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.3.1"

  set {
    name  = "installCRDs"
    value = true
  }
}
