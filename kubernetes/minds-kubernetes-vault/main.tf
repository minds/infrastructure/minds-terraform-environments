terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.16.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Environment = "minds-kubernetes-vault"
      Automation  = "terraform"
    }
  }
}

data "aws_eks_cluster" "this" { name = var.cluster_id }

data "aws_eks_cluster_auth" "this" { name = var.cluster_id }

provider "kubernetes" {
  host                   = data.aws_eks_cluster.this.endpoint
  token                  = data.aws_eks_cluster_auth.this.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.this.certificate_authority.0.data)
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.this.endpoint
    token                  = data.aws_eks_cluster_auth.this.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.this.certificate_authority.0.data)
  }
}
