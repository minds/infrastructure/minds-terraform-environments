resource "kubernetes_namespace" "traefik" {
  metadata {
    name        = "traefik"
    annotations = { name = "traefik" }
    labels      = { name = "traefik" }
  }
}

resource "helm_release" "traefik" {
  name       = "traefik"
  chart      = "traefik"
  namespace  = kubernetes_namespace.traefik.metadata[0].name
  repository = "https://helm.traefik.io/traefik"
  version    = "10.1.1"

  timeout = 1800

  values = [yamlencode({
    deployment = { kind = "DaemonSet" }
    # ports = { web = { redirectTo = "websecure" } }
    # service = {
    #   annotations = {
    #     "service.beta.kubernetes.io/aws-load-balancer-ssl-cert"  = ""
    #     "service.beta.kubernetes.io/aws-load-balancer-ssl-ports" = "443"
    #   }
    # }
  })]
}
