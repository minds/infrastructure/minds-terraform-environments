module "monitoring" {
  // Variables aren't supported for sources, so module versioning is a bit awkward.
  source = "git::https://gitlab.com/minds/infrastructure/minds-terraform-modules.git//minds-kubernetes-monitoring?ref=minds-kubernetes-monitoring-0.0.1&depth=1"

  prometheus_resources = {
    requests = { cpu = "500m", memory = "1Gi" },
    limits   = { cpu = "1000m", memory = "2Gi" }
  }

  prometheus_additional_values = yamlencode({
    prometheus = {
      service = {
        type = "LoadBalancer"
        annotations = {
          "service.beta.kubernetes.io/aws-load-balancer-internal" = "0.0.0.0/0"
        }
      }
    }
  })
}
